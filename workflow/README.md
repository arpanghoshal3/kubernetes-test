# Project workflow
The DevOps setup requires the developers to follow certain git workflow guidelines.

## TL;DR
```
FEATURE -> PUSH -> FEATURE = DEVELOPMENT
FEATURE -> MERGE -> DEVELOP = STAGING
DEVELOP -> MERGE -> MASTER = RELEASE
```

## Branch conventions
1. The default branch is `develop`. 
2. The release branch is `master`.

- All pushes made by developers on branches other than `master` and `develop` are deployed to their own personal **development environment**. This environment is avaiable under a specific domain prefixed with `YourGitlabUsername-develop`. Some useful information is usually displayed about this in the pipeline logs on gitlab if you are unsure where your deployment namespace or domain is.
- When a merge is made into develop from a feature branch and the pipeline succeeds, the app is deployed to the **staging environment**.
- When a merge is made from **develop into master** and the pipeline succeeds, the app is **deployed to production**. Additionally a release is generated in the gitlab repository which contains changelogs and general information about the release. Optionally you can add a semantic version `tag` to the merge request before merging, to get a version other than a date on the release.

When you are working on a feature or fix, **always branch out of develop**.

i.e
```
git status
>> on branch develop
git checkout -b feature_or_fix/new-stuff
```

## Rolling back a release
Following the workflow mentioned here will allow very simple rollbacks.

All you need to do is revert the merge that was made on develop or master and a new pipeline will run to reset the deployment.

## Deployment strategy
Deploying a release causes **no downtime** if there are more than 1 pods in the namespace. (The APP_REPLICAS variable in the gitlab ci variables says how many pods there are in a given namespace)

## NOTE:
A pipeline is **not triggered** if files and directories relevant to the deployment have no changes.

It is therefore important to regularly ensure that the `ci/rules.yml` is up to date with regards to that.