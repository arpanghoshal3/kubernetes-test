# Install dependencies
Requirements for using the terraform client.

**These are instructions for linux.**

## 1. Install the aws client
Installation guide: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

When your done installing you need to run `$ aws configure` and enter aws IAM security credentials which you can create in the aws IAM console.\
This way the aws client is connected to your amazon account. You can set the `region` to `eu-west-1`.

Aws client configuration guide: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html

## 2. Install asdf
The terraform client installation is best done through a package version manager called **asdf**.

> ```bash
> # Clone the repo
> git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.1
>
># Add to bash file
>echo '. $HOME/.asdf/asdf.sh' >> ~/.bashrc
>echo '. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc
>```

The installation instructions can be found here: https://asdf-vm.com/guide/getting-started.html#_1-install-dependencies

## 3. Install terraform with asdf
>```bash
>asdf plugin-add terraform https://github.com/Banno/asdf-hashicorp.git
># list all available terraform versions
>asdf list-all terraform
># install the latest version (As of now it is 1.1.2)
>asdf install terraform 1.1.2
># Make this version global on your machine
># you can also choose to have it local to the current directory by using 'local'
>asdf global terraform 1.1.2


# Preparation

Before running the script

## PEM file

A pem file contains a private key that can be linked to an EC2 instance. 
You will need this file stored on your machine in order to ssh into your EC2 instance after running the terraform script.

You have to create this key pair in the AWS EC2 console before you can generate the infrastructure with the terraform script. 
The `key_name` attribute below is referencing the name you gave to the key pair in aws.

The reference to the created key pair
```tf
resource "aws_instance" "IrisDevopsInstance" {
  ...
  key_name                    = "aegir-devops" # <- Replace with your key pair name
  ...
}
```
The AWS EC2 console - Created key pair
![aws ec2 key pair](images/create-pem.png)

Once you create the key pair, AWS will automatically download the file to your computer.\
**Store this file under ~/.aws/keys/{whatever-name-you-give-the-file}.pem**

Set the permissions on the file to read only access for only yourself (this is required):
`chmod 400 ~/.aws/keys/{whatever-name-you-give-the-file}.pem`

## Domain settings
The terraform script requires you to own a [route53 domain](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/registrar.html) on aws.
This allows us to have multiple environments in the cluster accessible via a subdomain name such as staging.{yourdomain}.com

Once you have a domain replace aegir-devops.com with whatever your domain name is in the script.
```
data "aws_route53_zone" "DevOpsZone" {
  name         = "aegir-devops.com" # <- Replace with your domain
  private_zone = false
}
```

# Using the terraform script
With the terraform script we can automatically generate our infrastructure on aws.

## Initializing the terraform state
This generates some meta files and initializes a terraform state used to manage changes.
You only have to do this once.
> `$ terraform init`

## Generating the plan
This outputs a binary file in the current directory to use to deploy the changes.
You have to run this every time you make changes.
> `$ terraform plan -out=plan.bin`

## Applying the changes
This pushes the changes to the account currently logged in through the aws client.
You have to run this every time you want to deploy changes that you have applied with the `plan` command.
> `$ terraform apply "plan.bin"`


# Connecting to the ec2 instance
Using the pem you created and stored earler you can now ssh into your domain.

> `$ ssh -i "~/.aws/keys/{your-pem-file}" ubuntu@{your-ssh-domain-url}`

For example 

> `$ ssh -i "~/.aws/keys/aegir-devops.pem" ubuntu@iris.aegir-devops.com`
