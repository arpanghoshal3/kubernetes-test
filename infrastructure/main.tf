# -------------------------------------------------------------------- #
# This generates a basic layout for a microk8s devops environment      #
# consisting of one ec2 instance. It is possible to use more than      #
# one instance with microk8s, simply create more than one aws_instance # 
# and associate it's IP address with the record lists                  # 
# -------------------------------------------------------------------- #

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

resource "aws_security_group" "DevOpsSecurityGroup" {

  # These are standard ingress rules defined for microk8s
  # cidr_blocks=["0.0.0.0/0"] means that anyone with a valid PEM can ssh
  # into the instance. The pem is a public/private key pair created
  # through aws cli/console

  name = "devops-security-group"

  // ssh
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // http
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // https
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // k8s
  ingress {
    from_port   = 16443
    to_port     = 16443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // egress
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# --------------------------------------------------------------------
# Declare a zone to use (This is a route53 registered domain on an AWS account)

data "aws_route53_zone" "DevOpsZone" {
  name         = "aegir-devops.com"
  private_zone = false
}

# --------------------------------------------------------------------
# Create an EC2 instance
# We can create more instances with their own kubernetes clusters
# the same way we do it with this one and route it to another sub-domain
# such as ros.{your-domain}.com

resource "aws_instance" "IrisDevopsInstance" {
  # The image for microk8s needs to be ubuntu
  # Ubuntu 18.04 from AWS - https://cloud-images.ubuntu.com/locator/
  ami                         = "ami-07d8796a2b0f8d29c"
  # This should be plenty even for the iris production environments
  instance_type               = "t3.medium"
  associate_public_ip_address = true
  # This keypair is created beforehand in amazon ec2 console
  # It is possible to generate it here but it will leave a trace in the terraform state
  key_name                    = "aegir-devops"
  vpc_security_group_ids = [
    aws_security_group.DevOpsSecurityGroup.id
  ]
  tags = {
    Name = "MicroK8s - IrisDevops"
  }
  root_block_device {
    volume_size = "32" # 32 GiB device storage
  }
}

# --------------------------------------------------------------------
# Create route records and associate with the EC2 instances IP address 
# These records are basically used to DNS resolve domain addresses     
# We are linking them all to our EC2 instance public dns name
# Later through microk8s we can route different services to these domains

# THIS IS THE SUB DOMAIN FOR THIS EC2 INSTANCE
# Record for route iris.aegir-devops.com
resource "aws_route53_record" "iris-main" {
  zone_id = data.aws_route53_zone.DevOpsZone.zone_id
  name    = "iris"
  type    = "CNAME"
  ttl     = "30"

  # Associate with the EC2 instance just created
  records = [
    aws_instance.IrisDevopsInstance.public_dns,
  ]
}

# Record for route x.iris.aegir-devops.com
resource "aws_route53_record" "iris-wildcard" {
  zone_id = data.aws_route53_zone.DevOpsZone.zone_id
  name    = "*.iris"
  type    = "CNAME"
  ttl     = "30"

  # Associate with the EC2 instance just created
  records = [
    aws_instance.IrisDevopsInstance.public_dns,
  ]
}

# Record for route iris.dashboard.aegir-devops.com
resource "aws_route53_record" "iris-dashboard" {
  zone_id = data.aws_route53_zone.DevOpsZone.zone_id
  name    = "iris.dashboard"
  type    = "CNAME"
  ttl     = "30"

  # Associate with the EC2 instance just created
  records = [
    aws_instance.IrisDevopsInstance.public_dns,
  ]
}

# Record for wildcard routes x.iris.staging.aegir-devops.com
resource "aws_route53_record" "iris-staging-wildcard" {
  zone_id = data.aws_route53_zone.DevOpsZone.zone_id
  name    = "*.iris.staging"
  type    = "CNAME"
  ttl     = "30"

  # Associate with the EC2 instance just created
  records = [
    aws_instance.IrisDevopsInstance.public_dns,
  ]
}

# Record for wildcard routes x.iris.integration.aegir-devops.com
resource "aws_route53_record" "iris-integration-wildcard" {
  zone_id = data.aws_route53_zone.DevOpsZone.zone_id
  name    = "*.iris.integration"
  type    = "CNAME"
  ttl     = "30"

  # Associate with the EC2 instance just created
  records = [
    aws_instance.IrisDevopsInstance.public_dns,
  ]
}

# Record for wildcard routes x.iris.capacity.aegir-devops.com
resource "aws_route53_record" "iris-capacity-wildcard" {
  zone_id = data.aws_route53_zone.DevOpsZone.zone_id
  name    = "*.iris.capacity"
  type    = "CNAME"
  ttl     = "30"

  # Associate with the EC2 instance just created
  records = [
    aws_instance.IrisDevopsInstance.public_dns,
  ]
}