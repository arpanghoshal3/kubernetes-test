# The scripts
The scripts are an essential part of the devops procedure. They mainly interact with the CI/CD environment and the K8s yml files.

## merge_k8s
This script merges together all yml files from a specific folder into one large yml file.

## create_k8s
This script expects 3 parameters.
1. `K8S_DIRECTORY` - The path to the K8S directory that you want to generate k8s yml from.
2. `CI_CD_PREFIX` - A prefix on variables that are general purpose and apply to all environments. In the gitlab ci settings I have set this to `CI_CD`.
3. `SERVICE_PREFIX` - A prefix on variables that belong only to a specific environment. In the gitlab ci settings I have set this to for example `DEV`, `STAGE`, `DEMO` etc.

You can view the ci variables that are set up in the gitlab repository under `settings -> CI/CD -> variables -> expand`

If you wanted to create the K8s yml files for the elixir app on the staging environment, the script would be executed as such:
`./scripts/create_k8s phoenix_app_ci_cd/hello/k8s CI_CD STAGE > outfile.yml`

The script will exit with an error if any of the required environment variables are missing.

## generate_tag
This script creates an image tag depending on the branch being committed on.

As of right now, when you commit on a branch other than master, the image tag generated will be `branchname-latest`,
and on master branch, `commitdate-SHA`.

Ideally this would be done with semantic versioning, but that is quite a bit more involved and 
usually requires storing some metadata and setting up a bot account to push to the repositories.