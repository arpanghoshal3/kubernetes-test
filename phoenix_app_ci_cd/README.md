
# Phoenix app configuration
Recently, phoenix has added a configuration file called `runtime.exs` which allows us to set configuration variables on runtime instead of compile time.
This is great, since this means we can now create one build to distribute to multiple environments!

## Environment
In runtime.exs we configure and fetch all variables we need to configure which are specific to a deployment.

There's alot going on in this configuration file so just take a peek at `config/runtime.exs`

## Dependencies
We need two dependencies for our phoenix app.

### 1. Libcluster
Libcluster will take care of all communication between nodes in the cluster. They have a default Kubernetes DNS module which is exactly what we need.
To set it up, we first add it to our app's `mix.exs` dependencies:

mix.exs

```ex
defp deps do
    [
      {:phoenix, "~> 1.6.5"},
      ...
      {:plug_cowboy, "~> 2.5"},
      {:libcluster, "~> 3.3"}, # ADDED
    ]
  end
```

Now we need to add the topology to the application, which describes how the cluster issuing is taken care of.
Again there's a few things included there and probably better you check out `lib/hello/application.ex`.

Note that the variable being fetched with `Application.get_env` is a runtime configured variable. 
We need it to be able to dynamically set the kubernetes service name that libcluster will connect to.

The `append_if` method is also added to help with setting the topology supervisor in the configuration.

### 2. ExCoveralls
This library allows us to test for coverage and generate a coverage report as well as a coverage badge in our gitlab repository.

In `mix.exs`

```ex
defp deps do
    [
      {:phoenix, "~> 1.6.5"},
      ...
      {:plug_cowboy, "~> 2.5"},
      {:libcluster, "~> 3.3"}, # ADDED
      {:excoveralls, "~> 0.14.4", only: :test}
    ]
  end
```
And in the project section in the same file:
```ex
def project do
    [
      ...
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end
```

We can run the coverage test with `mix coveralls`

Here you can see a sample setup of coveralls in a gitlab environment: https://gitlab.com/parroty/excoveralls-demo/-/blob/master
Read the readme instructions there on how to get the gitlab badge and take a look at the gitlab-ci file as well as the readme in raw format.

## Prod.exs

In `prod.exs`, we want to remove most of the things and leave only the following:
```ex
config :hello, HelloWeb.Endpoint, 
    cache_static_manifest: "priv/static/cache_manifest.json"

config :hello, Hello.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool_size: 10
```

Note that we set the `Ecto.Adapter` here, just like in the `dev.exs` file. The rest of the 
configuration that is present in the `dev.exs` file is placed in `runtime.exs` for the production config.

They are however otherwise pretty much identical which will give us better assurance that things will work when deploying after working locally.

## Rel files
There is some configuration that has to be done in the `rel` folder. If you do not have the `rel` folder already,
you can generate it with `mix release.init`.

In there we need to modify `env.sh.eex` to have the following:
```bash
export POD_A_RECORD=$(echo $POD_IP | sed 's/\./-/g')
export RELEASE_DISTRIBUTION=name
export RELEASE_NODE=${APP_NAME}@$(echo $POD_A_RECORD).$(echo $NAMESPACE).pod.cluster.local
```

This is needed for the libcluster setup.

Here `POD_IP` is the IP address that kubernetes assigned to the phoenix node.
In kubernetes terms, a `pod` can be thought of like a `node`. It is a running container in the cluster.

`NAMESPACE` is the isolated cluster space that the pod and any other resource created by 
the k8s reside within and we use a name (i.e `namespace`) to reference it.


## Generate the lockfile
After we have all our dependencies, we can create a lockfile. The lockfile locks all dependencies, including 
all dependencies of the dependencies, to a specific version. This ensures that our application does not update
any sub-dependencies without our know-so and prevents any erroneous patches from messing with our application.

To generate it we simply type:
`mix deps get`

To remove unused dependencies we can type:
`mix deps.clean --unused --unlock`

_That should cover the needed modifications to the application._

## K8s
The k8s are located under the `k8s` folder. Those are used to deploy the phoenix application and all the needed resources.

I will not be going into detail on those as they are pretty much going to be "magic" until you start struggling with it yourself.
Just note that all `{{SOMETHING}}}` are environment variables that are replaced via the create_k8s script.

For details on how to debug cluster resources refer to the `kuberenetes` readme. (otherwise google is always a good start).

# Dependencies (for creating your own phoenix app)
We will not be needing this right now but you can do this yourself. Also refer to the phoenix up and running documentation:
https://hexdocs.pm/phoenix/up_and_running.html
 
## Install
1. Install asdf
2. Install erlang latest version (24.2) with asdf
```
asdf plugin add erlang https://github.com/asdf-vm/asdf-erlang.git

apt-get -y install build-essential autoconf m4 libncurses5-dev libgl1-mesa-dev libglu1-mesa-dev libpng-dev libssh-dev unixodbc-dev xsltproc fop libxml2-utils libncurses-dev openjdk-11-jdk

asdf list-all erlang # Show all versions of erlang
asdf install erlang <latest_version> # install latest version of erlang
asdf global erlang <latest_version> # Set latest version as global version on machine
```

3. Install elixir latest version (1.13.1) with asdf
```
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
asdf install elixir <latest_version>
asdf global elixir <latest_version>
```

4. Install phoenix dependencies
```
mix local.hex --force
mix local.rebar --force
mix archive.install hex phx_new --force
```

5. Install postgres and pgadmin4 - if you want to run locally
You might need to configure the user postgres to have a password. For simplicities sake set it as 'postgres'. 
Helpful link: https://stackoverflow.com/a/26735105/13768310

# Run phoenix app

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
