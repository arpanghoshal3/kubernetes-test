defmodule Hello.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    # Get 'my_name' from runtime.exs configuration or use 'phoenix-app'
    name_or_default = Application.get_env(:Hello, :my_name, "phoenix-app")
    # https://hexdocs.pm/libcluster/Cluster.Strategy.Kubernetes.html#content
    topologies = [
      k8s: [
        strategy: Elixir.Cluster.Strategy.Kubernetes.DNS,
        config: [
          service: "#{name_or_default}-service-headless", # Same name configured in k8s/service.yml
          application_name: name_or_default,
          polling_interval: 10_000
        ]
      ]
    ]
    children = [
      # Start the Ecto repository
      Hello.Repo,
      # Start the Telemetry supervisor
      HelloWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Hello.PubSub},
      # Start the Endpoint (http/https)
      HelloWeb.Endpoint
      # Start a worker by calling: Hello.Worker.start_link(arg)
      # {Hello.Worker, arg}
    ]
    |> append_if(Application.get_env(:Hello, :is_production, false), {Cluster.Supervisor, [topologies, [name: Hello.ClusterSupervisor]]})

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Hello.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    HelloWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp append_if(list, condition, item) do
    if condition, do: list ++ [item], else: list
  end

end
