# Local setup
After having set up the infrastructure we can now set up the EC2 instance for Microk8.\
Microk8 is a lightweight kubernetes cluster managed inside an ec2 instance.

You can read more about Microk8s here: https://microk8s.io/

## Install kubectl
You will need kubectl on your machine in order to manage the kubernetes cluster locally

Follow this installation guide: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

## Kubectl config file setup
Kubectl does not have any nice way of allowing you to manage multiple
kubeconfig files (kubernetes clusters access configuration files), which means every time you set a config, the old one would be lost 
and you would have to get the config again from the cluster to access it with kubectl on your local machine.

To mitigate this we can do the following
### 1. Create a new directory
>```bash
># Create directory inside .kube
>mkdir ~/.kube/custom-contexts
>```
You should add all your kube configuration files to this directory with a .yml extension

### 2. Append script to bash
Add this to the bottom of `~/.bashrc`
>```bash
># Set the default kube context if present
>DEFAULT_KUBE_CONTEXTS="$HOME/.kube/config"
>if test -f "${DEFAULT_KUBE_CONTEXTS}"
>then
>export KUBECONFIG="$DEFAULT_KUBE_CONTEXTS"
>fi
>
># Additional contexts should be in ~/.kube/custom-contexts/ 
>CUSTOM_KUBE_CONTEXTS="$HOME/.kube/custom-contexts"
>mkdir -p "${CUSTOM_KUBE_CONTEXTS}" # Create if not exists
>
>OIFS="$IFS"
>IFS=$'\n'
>for contextFile in `find "${CUSTOM_KUBE_CONTEXTS}" -type f -name "*.yml"`  
>do
>    export KUBECONFIG="$contextFile:$KUBECONFIG"
>done
>IFS="$OIFS"
>```

# Kubernetes Instance configuration

Set up the kubernetes cluster on the ec2 instance.
Make sure you do every step in order as otherwise the ingress might fail to set up correctly.

## Install microk8s
```bash
# SSH into the ec2 instance
ssh -i "~/.aws/keys/{your-pem-file}" ubuntu@{your-ssh-domain-url}
# Update and upgrade
sudo apt update
sudo apt upgrade
# Install microk8s
# Important to note here is that we want to set a specific version using a channel
# This prevents snap from automatically updating and restarting the container
sudo snap install microk8s --classic --channel=1.21

# Give user permissions on microk8s
sudo usermod -a -G microk8s $USER
# Give user permissions to the kube config
sudo chown -f -R $USER ~/.kube
```

The permission changes will take effect upon restarting the ssh session.\
So exit the ssh session and reconnect.

## Enable microk8s features
```bash
# Wait for microk8s to be ready
microk8s status --wait-ready
# Enable microk8s features that we want
microk8s.enable dns ingress dashboard storage
```

## Enable your domain DNS for this instance
The instance has a sub domain associated with it as set up in the terraform script, f.x `iris.{your-domain}.com`.
In the microk8s configuration file we want to add this to the list of DNS's to allow
the domain name to be discovered via k8 files.

Add the following to `/var/snap/microk8s/current/certs/csr.conf.template`
```bash
# Open file in nano
nano /var/snap/microk8s/current/certs/csr.conf.template
```

Edit contents as follows:
```
DNS.1 = kubernetes
DNS.2 = kubernetes.default
DNS.3 = kubernetes.default.svc
DNS.4 = kubernetes.default.svc.cluster
DNS.5 = kubernetes.default.svc.cluster.local
DNS.6 = {your-sub-domain}.{your-domain}.com <<- ADD THIS
IP.1 = 127.0.0.1
IP.2 = 10.152.183.1
```

## Get cluster config file
Now we can take the configuration file from the instance and set it up on our machine. 
This will allow us to connect to the cluster from our local machine.

### 1. Get the config file
>```bash
># Get the kube config of the instance
># This will print it in the terminal
>microk8s config
>```

The config will look something like this:
>```yml
>apiVersion: v1
>clusters:
>- cluster:
>    certificate-authority-data: (Removed)
>    server: https://172.31.43.240:16443
>name: microk8s-cluster
>contexts:
>- context:
>    cluster: microk8s-cluster
>    user: admin
>name: microk8s
>current-context: microk8s
>kind: Config
>preferences: {}
>users:
>- name: admin
>user:
>    token: (Removed)
>```

Save this config file on your local machine under `~/.kube/custom-contexts/{choose-your-filename}.yml`\
For example  `~/.kube/custom-contexts/iris_aegir_devops_cluster.yml`

### 2. Modify the config file

Open this file and change the cluster name and config name to something more descriptive.

Also change the `server` attribute to `https://{your-sub-domain}.{your-domain}.com:16443`\
This is where the ingress files will be sent through to your cluster on calling kubectl apply

This is how it could look after the changes:
>```yml
>apiVersion: v1
>clusters:
>- cluster:
>    certificate-authority-data: (Removed)
>    server: https://iris.aegir-devops:16443
>name: iris-cluster
>contexts:
>- context:
>    cluster: iris-cluster
>    user: admin
>name: iris-config
>current-context: iris-config
>kind: Config
>preferences: {}
>users:
>- name: admin
>user:
>    token: (Removed)
>```

# Verify completion

## Verify that the bash script works
Check the available cluster configs:
> `kubectl config get-contexts`

```
CURRENT   NAME              CLUSTER
        iris-config       iris-cluster
```

Set the config as the active one:
>`kubectl config use-context iris-config`

Check the current context:
> `kubectl config current-context`

## Verify that the config is valid
> `kubectl get nodes`

You should see your EC2 instance listed.


# Adding HTTPS
To add https to our domain `{sub-domain}.{domain}.com` we can use let's encrypt.

## Install cert-manager to our cluster:
> `kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml`

In order for this to work you must have successfully added the cluster config to your local machine.

## Add the letsencrypt cluster issuer
Using the letsencrypt.yaml file we can now apply the cluster issuer.

> `kubectl apply -f letsencrypt.yaml`

Wait for the cluster status to be ready, you can check the status with this command:
> `kubectl describe clusterissuer letsencrypt`

## Adding tls to service deployment ingresses
Now that we have set up letsencrypt we can add tls to any service by adding this to the ingress files:

```yml
metadata:
  annotations:
    cert-manager.io/cluster-issuer: "letsencrypt"
spec:
  tls:
  - hosts:
    - "{the-url-you-want-the-deployment-on}"
    secretName: {your-service-name}-tls # Can be any name you want
```

See the [ingress.yaml](../example_service/ingress.yaml) in the example_service folder for reference.

## Debugging certificates
When you create a certificate, both a `secret` and a `certificate` should be created.
If the website is not secure after applying a TLS certificate you can debug like this:

Check that the certificate itself is ready:
> `kubectl get certificate -n <some_namespace>`

If it's not ready you can check the logs on the certificate which will most likely tell you the cause:
> `kubectl describe certificate <certificate_name> -n <some_namespace>`

If that's not enough, you can check the ingress controller logs:
> `kubectl get pods -n ingress`
> ```
>NAME                                      READY   STATUS    RESTARTS   AGE
>nginx-ingress-microk8s-controller-cp7nw   1/1     Running   0          8d 
> ```
> `kubectl logs nginx-ingress-microk8s-controller-cp7nw -n ingress`

## Debugging secrets
Check that the secrets is in the namespace
> `kubectl get secret -n <some_namespace>`

Describe secret:
> `kubectl describe secret <secret_name> -n <some_namespace>`

## Debugging services

Check for services:
> `kubectl get svc -n <some_namespace>`

Describe the service:
> `kubectl describe svc <service_name> -n <some_namespace>`

## Remote accessing pods
Get the list of pods:
> `kubectl get pods -n <some_namespace>`

SSH into a pod:
> `kubectl exec -it <name_of_pod> sh -n <some_namespace>`

This is basically accessing a container in interactive mode.

## Creating, deleting and modifying a namespace
Namespaces are isolated and they cannot communicate with other namespaces.

- You can create a new namespace:
  > `kubectl create namespace <name_of_new_namespace>`

- You can delete a namespace:
  > `kubectl delete namespace <name_of_namespace>`

  This deletes everything created for this namespace, **including any volumes**.
  Deleting namespaces is usually the easiest option when you need to reconfigure.

- You can delete specific resources:
  > `kubectl delete <resource_type> -n <some_namespace>`

  Resources are for example `certificate`, `secret`, `ingress`, `deployment`, `service`, `pvc`, `pv`, `configmap`.

- You can add new resources by applying yml files:
  > `kubectl apply -f some_new_resource.yml -n <some_namespace>`