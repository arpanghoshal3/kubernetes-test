[![pipeline status](https://gitlab.com/hauks96/kubernetes-test/badges/develop/pipeline.svg)](https://gitlab.com/hauks96/kubernetes-test/-/commits/develop)
[![Coverage Status]( http://gitlab.com/hauks96/kubernetes-test/badges/develop/coverage.svg)](https://gitlab.com/hauks96/kubernetes-test?branch=develop)


# Motivation
Familiarizing the developers with the devops setup.

This includes:
- AWS
- Terraform (Infrastructure as code)
- Kubernetes (Microk8s)
- Gitlab kubernetes integration
- Gitlab CI/CD (Auto devops integration)

# EC2 Kubernetes cluster setup
This is a guide for setting up a kubernetes cluster on an AWS EC2 instance using Microk8s.

Steps to follow:
1. [Infrastructure as code (Terraform and AWS)](infrastructure/)
2. [Kubernetes setup](kubernetes/)
3. [Deploy an example service](example_service/)

Follow the steps in the order given.

# Gitlab CI/CD  
This is a setup guide for integrating the kubernetes cluster with gitlab and setting up a CI/CD pipeline

Steps to follow:
1. [Gitlab kubernetes configuration](gitlab_configuration/)
2. [Gitlab ci/cd with phoenix app example](phoenix_app_ci_cd/)
3. [CI scripts](scripts/)

Follow the steps in the order given.

# Project workflow
1. [Project workflow](workflow/)